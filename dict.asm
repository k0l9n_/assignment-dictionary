global find_word
extern string_equals

section .text

find_word:
.loop:
	push rdi    ;start of string
	push rsi    ;start of rsi
	add rsi, 8
	
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jnz .yes
	
	mov rsi, [rsi]
	test rsi,rsi
	jnz .loop
	xor rax,rax
	jmp .return
.yes:
	mov rax, rsi
.return:
	ret
	
